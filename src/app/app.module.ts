import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppComponent } from './app.component';
import { BookComponent } from './book/book.component';
import { BookListComponent } from './book/book-list/book-list.component';
// @ts-ignore
import {AppRoutingModule} from './app-routing-module';
import {BookService} from './book/shared/book.service';
import {HttpClientModule} from '@angular/common/http';
import { BookDetailComponent } from './book/book-detail/book-detail.component';
import {FormsModule} from '@angular/forms';
import { BookCreateComponent } from './book/book-create/book-create.component';

@NgModule({
  declarations: [
    AppComponent,
    BookComponent,
    BookListComponent,
    BookDetailComponent,
    BookCreateComponent,
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    HttpClientModule,
    FormsModule,
  ],
  providers: [BookService],
  bootstrap: [AppComponent]
})
export class AppModule { }
