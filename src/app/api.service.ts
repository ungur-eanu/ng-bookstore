import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {Books} from './books';

@Injectable({
  providedIn: 'root'
})
export class ApiService {
  apiEndpoint = 'http://localhost:8080/api/';

  constructor(private httpClient: HttpClient) {
  }

  public getBooks() {
    return this.httpClient.get<Books>(`${this.apiEndpoint}/books`);
  }

  public deleteBook(id: number) {
    return this.httpClient.delete(`${this.apiEndpoint}/books/${id}`);
  }

}
