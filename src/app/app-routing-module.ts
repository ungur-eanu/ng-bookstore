// import { NgModule } from '@angular/core';
// import { Routes, RouterModule } from '@angular/router';
//
// const routes: Routes = [];
//
// @NgModule({
//   imports: [RouterModule.forRoot(routes)],
//   exports: [RouterModule]
// })
// export class AppRoutingModule { }


import {NgModule} from '@angular/core';
import {RouterModule, Routes} from '@angular/router';
import {BookComponent} from './book/book.component';

import {BookDetailComponent} from './book/book-detail/book-detail.component';
import {BookCreateComponent} from './book/book-create/book-create.component';


const routes: Routes = [
  // { path: '', redirectTo: '/home', pathMatch: 'full' },
  {path: 'books', component: BookComponent},
  {path: 'book/new', component: BookCreateComponent},
  {path: 'book/detail/:id', component: BookDetailComponent},

];



@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule {
}
