import { Injectable } from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {Observable} from 'rxjs';
import {Book} from './book';

import { map } from 'rxjs/operators';

@Injectable()
export class BookService {
  private booksUrl = 'http://localhost:8080/api/books';

  constructor(private httpClient: HttpClient) { }

  getBooks(): Observable<Book[]> {
    return this.httpClient
      .get<Array<Book>>(this.booksUrl);
  }

  getBook(id: number): Observable<Book> {
    return this.getBooks().pipe(
      map(books => books.find(book => book.id === id)));
  }

  update(book): Observable<Book> {
    const url = `${this.booksUrl}/${book.id}`;
    console.log(url);
    return this.httpClient
      .put<Book>(url, book);
  }

  create(author: string, title: string, isbn: string, price: number, quantity: number): Observable<Book> {
    const book = { author, title, isbn, price, quantity};
    return this.httpClient
      .post<Book>(this.booksUrl, book);
  }

  public delete(id: number) {
    return this.httpClient.delete(`${this.booksUrl}/books/${id}`);
  }
}
