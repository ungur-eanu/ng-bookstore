import { Component, OnInit } from '@angular/core';
import {Book} from '../shared/book';
import {BookService} from '../shared/book.service';
import {Router} from '@angular/router';

@Component({
  selector: 'app-book-list',
  templateUrl: './book-list.component.html',
  styleUrls: ['./book-list.component.css']
})
export class BookListComponent implements OnInit {

  errorMessage: string;
  books: Array<Book>;
  selectedBook: Book;

  constructor(private bookService: BookService,
              private router: Router) {
  }

  ngOnInit(): void {
    this.getBooks();
  }

  getBooks() {
    this.bookService.getBooks()
      .subscribe(
        books => this.books = books,
        error => this.errorMessage = error as any
      );
  }

  onSelect(book: Book): void {
    this.selectedBook = book;
  }

  gotoDetail(): void {
    this.router.navigate(['/book/detail', this.selectedBook.id]);
  }

  delete(book: Book): void {
    this.bookService.delete(book.id)
      .subscribe(_ => {
        this.books = this.books.filter(b => b !== book);
        if (this.selectedBook === book) {
          this.selectedBook = null;
        }
      });
  }

}
