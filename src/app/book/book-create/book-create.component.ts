import { Component, OnInit } from '@angular/core';
import {BookService} from '../shared/book.service';
import {Location} from '@angular/common';

@Component({
  selector: 'app-book-create',
  templateUrl: './book-create.component.html',
  styleUrls: ['./book-create.component.css']
})
export class BookCreateComponent implements OnInit {

  constructor(private bookService: BookService,
              private location: Location) { }

  ngOnInit() {
  }

  goBack(): void {
    this.location.back();
  }

  save(author, title, isbn, price, quantity) {
    if (!this.isValid(isbn, title, author, price, quantity)) {
      alert('ISBN, Author and Title are required');
      return;
    }
    this.bookService.create(author, title, isbn, price, quantity)
      .subscribe(_ => this.goBack());
  }

  private isValid( author, title, isbn, price, quantity) {
    if (!author || !isbn || !title) {
      console.log('ISBN, Author and Title are required');
      return false;
    }
    if (!Number.isInteger(Number(price))) {
      console.log('price has to be a number');
      return false;
    }

    if (!Number.isInteger(Number(quantity))) {
      console.log('Nr of Pages has to be a number');
      return false;
    }

    if (!(isbn.length === 13)) {
      console.log('ISBN has to be exactly 13 characters');
      return false;
    }
    return true;
  }
}
