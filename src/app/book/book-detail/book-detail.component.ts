import {Component, Input, OnInit} from '@angular/core';
import {BookService} from '../shared/book.service';
import {ActivatedRoute, Params} from '@angular/router';
import {Location} from '@angular/common';

import {switchMap} from 'rxjs/internal/operators';
import {Book} from '../shared/book';

@Component({
  selector: 'app-book-detail',
  templateUrl: './book-detail.component.html',
  styleUrls: ['./book-detail.component.css']
})
export class BookDetailComponent implements OnInit {

  @Input() book: Book;

  constructor(private bookService: BookService,
              private route: ActivatedRoute,
              private  location: Location) { }

  ngOnInit(): void {
    this.route.params.pipe(
      switchMap((params: Params) => this.bookService.getBook(+params.id)))
      .subscribe(book => this.book = book);
  }

  goBack(): void {
    this.location.back();
  }

  save(): void {
    console.log(this.book);
    this.bookService.update(this.book).subscribe(_ => this.goBack());
  }

}
