import {Book} from './book/shared/book';

export interface Books {
  books: Book[];
}
