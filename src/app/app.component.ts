// import {Component} from '@angular/core';
import {ApiService} from './api.service';
import {Book} from './book/shared/book';
import {Books} from './books';

// @Component({
//   selector: 'app-root',
//   templateUrl: './app.component.html',
//   styleUrls: ['./app.component.css']
// })
// export class AppComponent {
//   title = 'ng-bookstore';
//   books: Book[];
//
//   constructor(private apiService: ApiService) {
//   }
//
//   // tslint:disable-next-line:use-life-cycle-interface
//   ngOnInit() {
//     this.apiService.getBooks().subscribe((res: Books) => this.books = res.books);
//   }
// }


import { Component } from '@angular/core';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title = 'ng-bookstore';
}
